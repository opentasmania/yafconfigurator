# Open Tasmania yafconfigurator

------------------------------------------------------------------------

## Name

Yet Another Fine Configurator.

## Description

Everyone has a configurator. There are many like it, but this one is mine.

There are a [myriad of reasons](https://xkcd.com/927/) why the world needs another fine configuration system.

## Badges

![Pipeline](https://gitlab.com/opentasmania/yafconfigurator/badges/main/pipeline.svg)
![Coverage](https://gitlab.com/opentasmania/yafconfigurator/badges/main/coverage.svg)

## Installation

#### requirements.txt:

Add the following to your requirements.txt file  
`pyutils @ git+https://gitlab.com/opentasmania/yafconfigurator.git ; python_version >= "3.12" and python_version < "4.0"`

#### poetry:

In the **[tool.poetry.dependencies]** section of your pyproject.toml  
`pyutils = { git = "https://gitlab.com/opentasmania/yafconfigurator.git" }`

## Dependencies

| Requirement                                                          | Version     |  
|----------------------------------------------------------------------|-------------|
| [python](https://python.org)                                         | >=3.12,<4.0 |  
| [pytest](https://pytest.org/)                                        | ^8.2        |
| Open Tasmania [pyutils](https://gitlab.com/opentasmania/pyutils.git) | ^1          | 
| [filelock](https://github.com/tox-dev/filelock)                      | ^3          |
| [send2trash](https://github.com/arsenetar/send2trash)                | ^1.8        |  

## Example Code

Some [code](example_usage.py) is provided.

## Support

Please don't hesitate to use the [issues board](https://gitlab.com/opentasmania/yafconfigurator/-/issues) for asking
information.

## Roadmap

* YAML support
* SQLite support
* Postgres support
* CouchDB support

## Contributing

[Merge requests](https://gitlab.com/opentasmania/yafconfigurator/-/merge_requests) are always welcome, no matter how
small or seemingly crazy.

## Authors and acknowledgment

* Peter Lawler
* Creators of Python

## License

GNU Affero (AGPL) v3

## Project status

It has.