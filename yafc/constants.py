# -*- coding: utf-8 -*-
"""Yet Another Fine Configurator constants."""

# Copyright Peter Lawler <relwalretep@plnom.com>
# GNU Affero (AGPL) v3
__author__ = "Peter Lawler <relwalretep@gmail.com>"
__copyright__ = "Copyright Peter Lawler <relwalretep@gmail.com>"
__licence__ = "GNU Affero (AGPL) v3"

from logging import info
from os import getcwd
from os.path import expanduser, relpath

from pyutils.args import assign_parameter_from_arguments  # type: ignore

HOME_DIRECTORY = expanduser("~")
CURRENT_WORKING_DIRECTORY = getcwd()
RELATIVE_WORKING_DIRECTORY = relpath(
    CURRENT_WORKING_DIRECTORY, HOME_DIRECTORY
)

params = assign_parameter_from_arguments()

# Assign each variable only if it is currently undefined
PROJECT_NAME = params[0]
PROJECT_ROOT_PATH = params[1]
PROJECT_CONFIG_DIR = params[2]

PROJECT_CONFIG_DEFAULTS_DIR = f"{PROJECT_CONFIG_DIR}/defaults"

info(f"PROJECT_NAME: {PROJECT_NAME}")
info(f"PROJECT_ROOT: {PROJECT_ROOT_PATH}")
info(f"CONFIG_DIR: {PROJECT_CONFIG_DIR}")
info(f"CONFIG_DEFAULTS_DIR: {PROJECT_CONFIG_DEFAULTS_DIR}")
