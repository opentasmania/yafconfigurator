# -*- coding: utf-8 -*-
"""There are many configuration managers, this one is mine."""

# Copyright Peter Lawler <relwalretep@plnom.com>
# All rights reserved
__author__ = "Peter Lawler <relwalretep@plnom.com>"
__copyright__ = "Copyright Peter Lawler <relwalretep@plnom.com>"
__licence__ = "All rights reserved"

from copy import deepcopy
from json import dumps, load, dump, JSONDecodeError
from logging import warning, error, debug
from os.path import exists, join
from typing import Any, Dict, List, Optional

from pyutils.custom_types import JSON
from pyutils.masking import mask_sensitive_info

from yafc.constants import (
    PROJECT_CONFIG_DIR,
    PROJECT_CONFIG_DEFAULTS_DIR,
)

# from uuid_extensions import uuid7, uuid7str
# Ensure this module is installed or remove this line
# JSON typing in Python is a mess. See https://github.com/python/typing/issues/182 (opened 2016)

ConfigType = Any


class ConfigManager:
    """Class to manage configurations.

    Usage:
    config_manager = ConfigManager()  # initialize with default configuration file
    config_manager.set("key", "value")  # set a configuration value
    config_manager.save()  # save the configuration changes to file
    config_value = config_manager.get("key")  # retrieve a configuration value

    Attributes:
    - configs (dict): A dictionary containing all the configurations.

    Methods:
    - __init__(config_name: str = "yafc_config", config_file: str = "yafc_config.json")
      Initializes the ConfigManager with the configuration loaded from the given file.

    - load(config_file) -> Dict[str, Any]
      Load and return a configuration file.

    - get_all_configs() -> dict
      Retrieve all the configurations.

    - get_config(config_name: str) -> Any
      Retrieve the value associated with the given configuration name.

    - get(config_name: str, key: str, default=None) -> Any
      Get the value corresponding to the given key from the specified config.

    - get_config_value_by_name(key: str, category_name: str = None) -> Any
      Retrieves a configuration value by key.

    - set(key: str, value: Any) -> None
      Sets a value in the configuration.

    - save() -> None
      Saves the current configuration back to the file.

    - reset() -> None
      Resets the configuration to its original state.
    """

    def __init__(
        self,
        config_name: str = "yafc_config",
        config_file: str = "yafc_config.json",
    ):
        """Initialize the ConfigManager with the configuration loaded from the given file.

        :param config_file: The name of the configuration file, default is "yafc_config.json".
        """
        # Creating an empty dictionary to store the original configuration
        self.original_config = {}
        # If `configs` attribute is not present, create an empty dictionary for it
        if not hasattr(self, "configs"):
            self.configs: ConfigType = {}
        # If the configuration name already exists in configs, stop executing further and return
        elif config_name in self.configs:
            return

        # If config_file argument is passed, load that file's content as the configuration for config_name
        if config_file:
            self.configs[config_name] = self.load(config_file)
            # Create a deep copy of loaded configuration as the original configuration
            self.original_config[config_name] = deepcopy(
                self.configs[config_name]
            )
            # Produce debug level log with masked sensitive info from loaded configuration
            debug(
                f"Config loaded: {dumps(mask_sensitive_info(self.configs[config_name]))}"
            )
        # If config_file argument is not passed, assign a default file path using config_name
        else:
            config_file = join(PROJECT_CONFIG_DIR, config_name)
        # Storing config_file path and config_name for future use
        self.filepath = config_file
        self.config_name = config_name

    def __getitem__(self, config_name):
        """Get an item.

        Args:
            config_name: A string representing the name of the configuration.

        Returns:
            The configuration specified by the config_name.

        Raises:
            ValueError: If the configuration does not exist in the configs dictionary.
        """
        try:
            return self.configs[config_name]
        except KeyError as err:
            raise ValueError("The configuration does not exist.") from err

    def get_deep(self, config_name: str, keys: List[str]) -> JSON:
        """Get keys from a deep config.

        Args:
            config_name: A string representing the name of the configuration
            keys: A list of strings representing the keys to retrieve nested values from the configuration

        Returns:
            Any: The value corresponding to the nested keys in the configuration, or None if not found

        """
        result = self.configs.get(config_name, {})
        for key in keys:
            result = result.get(key, None)
        return result

    def set_deep(self, config_name: str, keys: List[str], value: Any) -> None:
        """Set a deep config.

        Args:
            config_name (str): The name of the configuration.
            keys (List[str]): The list of keys to navigate the nested dictionary.
            value (Any): The value to set at the specified location.

        Raises:
            ValueError: If the configuration does not exist.

        Returns:
            None

        """
        if config_name not in self.configs:
            raise ValueError("The configuration does not exist.")

        temp = self.configs[config_name]
        for key in keys[:-1]:
            if key not in temp:
                temp[key] = {}
            temp = temp[key]
        temp[keys[-1]] = value

    def load(self, config_file) -> Dict[str, Any]:
        """
        Load and return a configuration file.

        :param config_file: The name of the configuration file.
        :return: The loaded configuration as a dictionary.
        :raises FileNotFoundError: If the configuration file is not found and no default is available.
        :raises JSONDecodeError: If the configuration file has an invalid JSON format.
        :raises Exception: If an error occurs while loading the config file.
        """
        config_path = join(PROJECT_CONFIG_DIR, config_file)
        debug(f"Loading config from {config_path}")
        if not exists(config_path):
            warning(
                f"Custom {config_file} config not found in {PROJECT_CONFIG_DIR}, looking for default"
            )
            config_path = join(PROJECT_CONFIG_DEFAULTS_DIR, config_file)
        if not exists(config_path):
            raise FileNotFoundError(
                f"Config file {config_file} not found and no default available in {PROJECT_CONFIG_DEFAULTS_DIR}"
            )
        try:
            with open(config_path, "r") as file:
                config = load(file)
                masked_config = mask_sensitive_info(config)
                debug(
                    f"Configuration loaded: {dumps(masked_config, indent=2)}"
                )
                return config
        except JSONDecodeError as e:
            error(f"Invalid JSON format: {e}")
            raise
        except Exception as e:
            error(f"An error occurred while loading the config file: {e}")
            raise

    def get_all_configs(self) -> dict:
        """
        Retrieve all the configurations.

        :return: All configurations as a dictionary.
        """
        return self.configs

    def get_config(self, config_name: str) -> JSON:
        """
        Retrieve the value associated with the given configuration name.

        :param config_name: The name of the configuration to retrieve.
        :return: The value associated with the configuration name, or None if the configuration name does not exist.
        """
        return self.configs.get(config_name, None)

    def get(self, config_name: str, key: str) -> JSON:
        """
        Get the value corresponding to the given key from the specified config.

        :param config_name: The name of the config from which to retrieve the value.
        :param key: The key for which the value needs to be retrieved.
        :return: The value corresponding to the given key, or the default value if the key is not found.
        :raises KeyError: If the key is not found in the given config.
        """
        if key in self.configs[config_name]:
            return self.configs[config_name].get(key)
        else:
            raise KeyError(f"'{key}' not found in the configuration.")

    def get_config_value_by_name(
        self, key: str, category_name: Optional[str] = None
    ) -> Any:
        """Retrieve a configuration value by key.

        Note: If a category_name is provided, it retrieves the value from the specific category configuration.

        :param key: The key to retrieve the value for.
        :param category_name: Optional; the name of the category to retrieve the value from.
        :return: The value associated with the key, or None if not found.
        """
        if category_name:
            category_data = next(
                (
                    item
                    for item in self.configs
                    if item["name"] == category_name
                ),
                None,
            )
            if not category_data:
                return None
            return category_data.get(key, None)
        return next((item[key] for item in self.configs if key in item), None)

    def set(self, config_name: str, key: str, value: JSON) -> None:
        """Set the value of a key in a specific configuration.

        Args:
            self: The instance of the class.
            config_name: A string representing the name of the configuration.
            key: A string representing the key to be set.
            value: A value to be assigned to the key. The type can be int, str, or float.

        Raises:
            KeyError: If the specified key is not found in the configuration.

        Returns:
            None
        """
        if config_name in self.configs:
            self.configs[config_name][key] = value
        else:
            raise KeyError(f"'{config_name}' not found in the configuration.")

    def save(self) -> None:
        """Save the current configuration back to the file."""
        with open(self.filepath, "w") as file:
            dump(self.configs, file, indent=2)

    def save_as(self, save_as_filename: str) -> None:
        """Save the current configuration back to the file."""
        with open(save_as_filename, "w") as file:
            dump(self.configs, file, indent=2)

    def reset(self) -> None:
        """Reset the configuration to its original state."""
        self.configs = self.original_config.copy()
