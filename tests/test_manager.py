# -*- coding: utf-8 -*-
"""Test Yet Another Fine Configurator manager."""

# Copyright Peter Lawler <relwalretep@plnom.com>
# GNU Affero (AGPL) v3
__author__ = "Peter Lawler <relwalretep@gmail.com>"
__copyright__ = "Copyright Peter Lawler <relwalretep@gmail.com>"
__licence__ = "GNU Affero (AGPL) v3"

import json
import logging
from os import remove
from os.path import join
from copy import deepcopy

import pytest

from yafc.constants import CURRENT_WORKING_DIRECTORY
from yafc.manager import ConfigManager

logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
)

# Set the path to the test configurations
test_config_path = join(f"{CURRENT_WORKING_DIRECTORY}", "config")


def test_init_DefaultConfigManager():
    """Test the initialization of the DefaultConfigManager.

    The method creates an instance of the ConfigManager class with a given configuration file path.
    It then asserts that the created object is an instance of the ConfigManager class.

    Returns:
        None
    """
    manager = ConfigManager(
        config_file=join(test_config_path, "test_config.json")
    )
    assert isinstance(manager, ConfigManager)


# assert manager.get_all_configs() == {}


def test_load_valid_file():
    """Test the load_valid_file method of the ConfigManager class.

    This method tests whether the ConfigManager can successfully load a valid configuration file and
     return the expected output.

    Parameters:
        None

    Returns:
        None

    Raises:
        AssertionError: If the expected output of the ConfigManager.get_all_configs() method
         does not match the expected_output.

    Example:
        test_load_valid_file()
    """
    manager = ConfigManager(
        config_file=join(test_config_path, "test_config.json")
    )
    expected_output = {"yafc_config": {"key": "value"}}
    assert manager.get_all_configs() == expected_output


def test_load_invalid_file():
    """Test case for loading an invalid file in the ConfigManager class.

    Raises:
        FileNotFoundError: If the specified config file does not exist.

    Usage:
        test_load_invalid_file()
    """
    with pytest.raises(FileNotFoundError):
        ConfigManager(config_file=join(test_config_path, "not_exist.json"))


def test_get_config_exists():
    """Test case for the `get_config_exists` method.

    Tests the functionality of the `get_config_exists` method in the `ConfigManager` class.

    Returns:
        None

    Raises:
        AssertionError: If the returned config value does not match the expected value.
    """
    manager = ConfigManager(
        config_file=join(test_config_path, "test_config.json")
    )
    assert manager.get_config("yafc_config")["key"] == "value"


def test_get_config_not_exists():
    """Test the behavior of `get_config` method when the config does not exist.

    The method creates a `ConfigManager` object with a test config file, and then calls the `get_config`
     method with a non-existing config key.
    The expected behavior is that the `get_config` method should return `None`.

    """
    manager = ConfigManager(
        config_file=join(test_config_path, "test_config.json")
    )
    assert manager.get_config("not_exist") is None


def test_set_config():
    """Test method for setting a new key-value pair in the config.

    :return: None
    """
    manager = ConfigManager(
        config_file=join(test_config_path, "test_config.json")
    )
    manager.set("yafc_config", "new_key", "new_value")
    assert manager.get_config("yafc_config")["new_key"] == "new_value"


def test_save_as_config():
    """Test the save_as_config method of the ConfigManager class.

    This method is responsible for saving the modified configuration to a new file.

    Returns:
        None

    Example:
        manager = ConfigManager(
           config_file=join(test_config_path, "test_config.json")
         )
        manager.set("yafc_config", "new_key", "new_value")
        manager.save_as(join(test_config_path, "saved_config.json"))
        with open(join(test_config_path, "saved_config.json"), "r") as f:
             saved_config = json.load(f)
        assert saved_config.get("yafc_config")["new_key"] == "new_value"
        remove(join(test_config_path, "saved_config.json"))

    """
    manager = ConfigManager(
        config_file=join(test_config_path, "test_config.json")
    )
    manager.set("yafc_config", "new_key", "new_value")
    manager.save_as(join(test_config_path, "saved_config.json"))
    with open(join(test_config_path, "saved_config.json"), "r") as f:
        saved_config = json.load(f)
    assert saved_config.get("yafc_config")["new_key"] == "new_value"
    remove(join(test_config_path, "saved_config.json"))


def test_reset_config():
    """Test case for reset method of ConfigManager.

    This method tests the functionality of the reset method in the ConfigManager class. It verifies
    that the reset method resets the configuration to its original state.

    :return: None
    """
    manager = ConfigManager(
        config_file=join(test_config_path, "test_config.json")
    )
    original_config = deepcopy(manager.get_all_configs())
    manager.set("yafc_config", "new_key", "new_value")
    manager.reset()
    assert manager.get_all_configs() == original_config
