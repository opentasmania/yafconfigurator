# -*- coding: utf-8 -*-
"""Yet Another Fine Configurator example."""

# -*- coding: utf-8 -*-
# Copyright Peter Lawler <relwalretep@plnom.com>
# GNU Affero (AGPL) v3
__author__ = "Peter Lawler <relwalretep@gmail.com>"
__copyright__ = "Copyright Peter Lawler <relwalretep@gmail.com>"
__licence__ = "GNU Affero (AGPL) v3"


from yafc.constants import PROJECT_CONFIG_DEFAULTS_DIR, PROJECT_CONFIG_DIR

from yafc.manager import ConfigManager


def main():
    """
    Entry point function for the application.

    This function performs the following steps:
    1. Finds the project root directory.
    2. Changes the current working directory to the project root.
    3. Initializes a ConfigManager instance with a specified configuration file.
    4. Loads the configuration from the specified file.
    5. Modifies the loaded configuration.
    6. Saves the modified configuration to a new file.

    Raises:
        FileNotFoundError: If the configuration file is not found.
        PermissionError: If there is insufficient permission to access or write the config file.
        Exception: If any other error occurs during the process.
    """
    config_data = ConfigManager(
        config_file=f"{PROJECT_CONFIG_DEFAULTS_DIR}/example_config.json"
    )

    try:
        print("Loaded config:")
        print(config_data.configs)
        print("Original config:")
        print(config_data.original_config)

        config_data.configs["default_config"]["setting"] = "new_value"
        config_data.configs["default_config"]["feature1"] = "disabled"

        # Save the modified configuration
        config_data.save_as(f"{PROJECT_CONFIG_DIR}/example_config.json")
        print("Modified Config Saved Successfully!")

    except FileNotFoundError:
        print("Config file not found.")
    except PermissionError:
        print(
            "Error: Insufficient permission to access or write the config file."
        )
    except Exception as e:
        print(f"An error occurred: {str(e)}")


if __name__ == "__main__":
    main()
